/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * This file is part of Nautilus Kill Thumbs.
 * https://gitlab.gnome.org/glerro/nautilus-kill-thumbs
 *
 * nautilus-kill-thumbs.c
 *
 * Copyright (c) 2022-2023 Gianni Lerro {glerro} ~ <glerro@pm.me>
 *
 * Nautilus Kill Thumbs is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Nautilus Kill Thumbs is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Nautilus Kill Thumbs. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2022-2023 Gianni Lerro <glerro@pm.me>
 */

#include "config.h"

#include "nautilus-kill-thumbs.h"

#include <glib.h>
#include <glib/gstdio.h>
#include <nautilus-extension.h>

static GType nautilus_kill_thumbs_type = 0;

static GObjectClass *parent_class;

static NautilusOperationResult
nautilus_kill_thumbs_update_file_info (NautilusInfoProvider     *info_provider,
                                       NautilusFileInfo         *nautilus_file,
                                       GClosure                 *update_complete,
                                       NautilusOperationHandle **operation_handle)
{
    gchar *filename = nautilus_file_info_get_name (nautilus_file);

    if (g_str_equal (filename, "Thumbs.db"))
    {
        gchar *uri = nautilus_file_info_get_uri (nautilus_file);
        gchar *filename = g_filename_from_uri (uri, NULL, NULL);
        g_free (uri);

        if (g_unlink (filename) != -1)
        {
            nautilus_file_info_add_emblem (nautilus_file, "unreadable");
            nautilus_file_info_invalidate_extension_info (nautilus_file);
        }
        else
        {
            g_free (filename);
            return NAUTILUS_OPERATION_FAILED;
        }

        g_free (filename);
    }

    return NAUTILUS_OPERATION_COMPLETE;
}

static void
nautilus_kill_thumbs_type_info_provider_iface_init (NautilusInfoProviderInterface *iface,
                                                    gpointer                      *iface_data)
{
    iface->update_file_info = nautilus_kill_thumbs_update_file_info;
}

static void
nautilus_kill_thumbs_class_init (NautilusKillThumbsClass *nautilus_kill_thumbs_class,
                                 gpointer                 class_data)
{
    parent_class = g_type_class_peek_parent (nautilus_kill_thumbs_class);
}

void
nautilus_kill_thumbs_register_type (GTypeModule *module)
{
    static const GTypeInfo info =
    {
        sizeof (NautilusKillThumbsClass),
        (GBaseInitFunc) NULL,
        (GBaseFinalizeFunc) NULL,
        (GClassInitFunc) nautilus_kill_thumbs_class_init,
        (GClassFinalizeFunc) NULL,
        NULL,
        sizeof (NautilusKillThumbs),
        0,
        (GInstanceInitFunc) NULL,
        (GTypeValueTable *) NULL
    };

    nautilus_kill_thumbs_type = g_type_module_register_type (
        module,
        G_TYPE_OBJECT,
        "NautilusKillThumbs",
        &info,
        0
        );

    static const GInterfaceInfo type_info_provider_iface_info =
    {
        (GInterfaceInitFunc) nautilus_kill_thumbs_type_info_provider_iface_init,
        (GInterfaceFinalizeFunc) NULL,
        NULL
    };

    g_type_module_add_interface (
        module,
        nautilus_kill_thumbs_type,
        NAUTILUS_TYPE_INFO_PROVIDER,
        &type_info_provider_iface_info
        );
}

GType
nautilus_kill_thumbs_get_type (void)
{
    return nautilus_kill_thumbs_type;
}

